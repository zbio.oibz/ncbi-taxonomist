%-------------------------------------------------------------------------------
%  \author Jan Piotr Buchmann <jan.buchmann@sydney.edu.au>
%  \copyright 2020
%-------------------------------------------------------------------------------

\documentclass[a4paper]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{lmodern}
\usepackage{natbib}
\usepackage{xspace}
\usepackage[tracking=true]{microtype}
\usepackage{amssymb,amsmath}
\usepackage{breakurl}
\usepackage{url}
\usepackage[shortcuts]{extdash}
\usepackage{setspace}
\usepackage{graphicx}
\usepackage[x11names, dvipsnames, table, cmyk]{xcolor}
\usepackage{lineno}
\usepackage{nth}
\usepackage{csquotes}
\usepackage[breaklinks, pdfborder={0 0 0}]{hyperref}

\newcommand{\thetool}{\texttt{ncbi-taxonomist}\xspace}
\newcommand{\taxodb}{\textquote{Taxonomy Database}\xspace}
\newcommand{\thecommand}[1]{\thetool \texttt{#1}}


\begin{document}

  \section{Introduction}
    Obtaining and managing taxonomic information and relationships is a
    critical part of every biological study. Here, we present \thetool, a
    Python command line tool facilitating the retrieval and handling of
    taxonomic information from the \taxodb \citep{Sayers2019} at the
    National Center for Biotechnology Information (NCBI). The taxonomic
    information is stored as cladogram based on phylogenetic concepts and
    encodes  hierarchical relationships, in contrast to a phylogenetic tree
    including evolutionary relationships.

    Each node in the cladogram is assigned an unique integer, the taxid,  which
    can be used to retrieve the corresponding information, e.g. the scientific
    name, rank, and parent node, among others. Conveniently, assigned taxids are
    commonly shared between life science databases. For example, taxids from
    the \taxodb at NCBI fetch the same taxa from Uniprot\citep{Uniprot2019}.
    The taxonomic information from the \taxodb can  be queried and retrieved
    via Entrez \citep{Sayers2019}, allowing to conduct efficient and specific
    data queries because the taxid links to retrieve related information from
    other Entrez databases, e.g. the taxid for \textit{Homo sapiens}, 9606, can
    be used to find human nucleotide or protein sequences.

    The \taxodb can be queried via Entrez to map taxonomic names to taxids and
    vice versa, link taxids to different entries, obtaining sub-trees, i.e. all
    children taxids for specific taxid, and to construct more advanced taxonomy
    queries using boolean operators. These queries can be done on the NCBI
    website or via the E-Utilities \citep{Kans2018}. The former approach is
    unfeasible to automate when performing multiple queries while the latter
    requires some form of scripting to overcome download limitations and
    extract the required information. \thetool is a command-line tool which can
    be integrated into scripts when required and uses
    \texttt{entrezpy}\citep{Buchmann2019} to communicate with Entrez,
    facilitating the interaction with Entrez.

    Existing software which requires taxonomic data, e.g. ETE3
    \citep{Huerta-Cepas2016}, is specialized for phylogenetic analysis and
    requires to download the whole taxonomic database in advance from NCBI's
    ftp server when handling taxonomic data from NCBI
    (\url{https://ftp.ncbi.nih.gov/pub/taxonomy/}. The current size of the
    compressed taxonomic database download is approximately 50~MB (Megabyte)
    and can be considered small. However, the download consists of several
    files resulting in approximately 354~MB and contains the dumps of the
    taxonomy database tables. This file needs to be kept up-to-date
    with every update. This increases complexity to parse and maintain local
    taxonomy databases.

    In contrast, \thetool can retrieve taxonomic information on-demand and does
    not require to download the whole taxonomic database, but has the ability
    to locally store taxonomic information. \thetool implements all operations
    which can be performed in NCBI's \taxodb in addition to creating user
    defined groups for selected taxa and a more versatile sub-tree command.

    \thetool is written in Python 3 ($\ge 3.8$) and the only dependency outside
    of the Python standard library is \texttt{entrezpy}. Because we develop and
    maintain \texttt{entrezpy}, which itself has no external dependencies,
    \thetool is less prone to suffer dependency hell
    (\url{https://en.wikipedia.org/wiki/Dependency_hell}) and remain stable and
    available.

    \thetool is licensed under the GNU General Public License v3 (GPL v3) and
    can be downloaded from PyPi. The source code is available on GitLab
    (\url{https://gitlab.com/janpb/ncbi-taxonomist}) and the documentation on
    Read the Docs (\url{https://ncbi-taxonomist.readthedocs.io/en/latest/}).
    In addition, a Docker container and Singularity image for \thetool
    including jq are available at ADDRESS.


  \section{ncbi-taxonomist}
    \thetool has six commands (see below) of which five can be linked together
    via pipes on *NIX systems to create more advanced pipelines. Results from
    \thetool queries are JSON objects and sent to standard output (Fig. 1A, B).
    This simplifies writing processing tools, e.g. viewers, or using existing
    tools to process JSON data, e.g. \texttt{jq}
    (\url{https://stedolan.github.io/jq/}). Individual \thetool commands can be
    chained together using pipes to create powerful, taxonomy related pipelines
    for automated taxonomic retrieval and management. The results of such
    pipelines can be used to create highly specific data sets for subsequent
    analysis steps. Several commands with the option to fetch data remotely
    from \taxodb can be used together with a local database, in which case the
    local query will run first. Local queries not producing a result will be
    tried remotely in such a situation.

    %\input{figs/ncbitaxonomist.fig.tex}

    \paragraph{\thecommand{collect}} collects taxonomic information remotely
    from NCBI's \taxodb and converts each taxon into a corresponding JSON object.
    It accepts taxids and taxon names as input.

    \paragraph{\thecommand{map}} maps taxids to taxon names and vice versa. In
    addition, it can map sequence and protein accessions to taxids. Currently,
    accessions from the following Entrez database are supported: assembly,
    bioproject, nucleotide, and protein.

    \paragraph{\thecommand{resolve}} resolves the lineages for taxids and taxon
    names (Figure 1C). To resolve accessions, a simple pipeline consisting of a
    mapping and resolving step can be created (Figure 1D).

    \paragraph{\thecommand{subtree}} extracts all lineages for specific ranks
    from  given taxids or taxon names. If only one rank is given, only the
    taxonomic information for this rank is extracted. If an upper, closer to
    the root, rank is specified, all lineages from the given rank to the lowest
    rank are returned. If a lower, further from the root, rank is given, all
    lineages from the lower rank to the root are returned. If an upper and
    lower rank are given, all lineages between these two ranks are returned.

    \paragraph{\thecommand{group}} adds taxa obtained from a query to a
    user-specified group. Each taxon can belong to several groups. This allows
    to create specific collections based on non-taxonomic relations (Figure 1E).

    \paragraph{\thecommand{import}} reads query results from standard input and
    stores the taxa locally in an SQLite database. This allows the creation of
    experiment-specific taxonomy databases. \texttt{import} stores the parsed
    taxa while printing the received input to standard output and therefore
    does not need to be the last step of a \thetool pipeline (Figure 1E).

    The versatility of \thetool allows it to be used for quick look-ups or to
    integrate it into more complex pipelines to manage or create experiment
    specific taxonomic subsets. The possibility to link taxonomic information
    to related data sets on NCBI via existing tools make \thetool a powerful
    tool for collecting and managing taxonomic data.


  \bibliography{references}
  \bibliographystyle{tpj}
\end{document}
