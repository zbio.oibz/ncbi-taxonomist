.. _resolve:

resolve
=======

``ncbi-taxonomist resolve`` resolve the lineage of a given taxid, name, or
accession. To resolve accessions, a :ref:`preceding mapping step
<resolvelineage>` is required.



.. code-block:: shell

  ncbi-taxonomist resolve [-h] [-t [<taxid> [<taxid> ...]]]
                               [-n [<name> [<name> ...]]] [-db <path>] [-r]
                               [-e <email>] [-m]

  -h, --help                            show this help message and exit
  -t, --taxids [<taxid> [<taxid> ...]]  Comma / space separated taxids
  -n, --names  [<name> [<name> ...]]    Comma separated names: 'Homo sapiens, Influenza B
                                        virus (B/Acre/121609/2012)'
  -db, --database <path>                Path to local database
  -r, --remote                          Use Entrez server
  -e, --email <email>                   Email, required for remote queries
  -m, --mapping                         Resolve accessions mapping result from map via STDIN
