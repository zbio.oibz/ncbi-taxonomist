.. _import:


import
======

``ncbi-taxonomist import`` stores taxonomic information and/ot accessions data
in a local database.

..  code-block:: shell

  ncbi-taxonomist collect <options>

  -h, --help                show this help message and exit
  -db, --database <path>    path to SQLite database
  -m, --mapping             read an acceesion-taxid mapping result from map via STDIN
  -l, --lineage             import lineages from resolve via STDIN
  -f, --filter <attribute>  Set attribute to print to STDOUT after import: accs, taxid, lin
