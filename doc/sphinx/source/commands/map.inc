.. _map:

map
===

``ncbi-taxonomist map`` maps taxonomic identifiers, e.g. taxid or names, and
accessions to the corresponding taxonomic node and its metadata.

.. code-block:: shell

  ncbi-taxonomist map <options>

  -t, --taxids    [<taxid> [<taxid> ...]]   Comma or space separated taxids
  -n, --names     [<name> [<name> ...]]     Comma separated names, e.g.
                                            'Homo sapiens,Influenza B virus (B/Acre/121609/2012)'
  -db, --database <path>                    Path to local database
  -r, --remote                              Use Entrez server
  -e, --email       <email>                 Email, required for remote queries
  -a, --accessions  [<acces> [<acces> ...]] Comma / space separated accessions
  -edb, --entrezdb  <entrezdb>              Entrez database for accessions. Default: nucleotide
