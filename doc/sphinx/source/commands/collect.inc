.. _collect:

collect
=======

``ncbi-taxonomist collect`` collects taxonomic information remotely from
Entrez. This is mainly useful to populate a local database. Interogating local
database is doen with with :ref:`map` or :ref:`resolve`.

.. code-block:: shell

  ncbi-taxonomist collect <options>

  -h, --help            show help message exit
  -t, --taxids          taxids, comma or space separated, e.g. -t 2, 9606, 122929
  -n, --names           comma separated list of names, e.g. 'Homo sapiens, Influenza B virus (B/Acre/121609/2012)'
  -e, --email <email>   email required for remote NCBI Entrez queries
