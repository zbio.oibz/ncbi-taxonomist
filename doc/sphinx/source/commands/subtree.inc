.. _subtree:

subtree
=======

``ncbi-taxonomist subtree`` collects taxonomic subsamples from a local or remote
database.

.. code-block:: shell

  ncbi-taxonomist subtree -h

Example
-------

.. code-block:: shell

  ncbi-taxonomist subtree
