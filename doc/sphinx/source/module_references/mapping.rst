.. include:: ../global.rst
.. _mapping_mod:

Mappers
=======
Mappers handle the mapping of taxids, names, and accessions to
each other.

Mapper: :mod:`ncbi_taxonomist.map.mapper`
-----------------------------------------
.. automodule:: ncbi_taxonomist.map.mapper
  :members:
  :undoc-members:

Remote mapper: :mod:`ncbi_taxonomist.map.remotemapper`
------------------------------------------------------
.. automodule:: ncbi_taxonomist.map.remotemapper
  :members:
  :undoc-members:

Remote accession mapper: :mod:`ncbi_taxonomist.map.remoteaccessionmapper`
-------------------------------------------------------------------------
.. automodule:: ncbi_taxonomist.map.remoteaccessionmapper
  :members:
  :undoc-members:
