.. _utils:

Utility functions used across modules
=====================================

Utility functions: :mod:`ncbi_taxonomist.utils`
-----------------------------------------------
.. automodule:: ncbi_taxonomist.utils
  :members:
  :undoc-members:
