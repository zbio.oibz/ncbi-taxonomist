.. include:: ../global.rst
.. _db_mod:

Database
========

Database modules for a local |basename_tt| database

Database manager: :mod:`ncbi_taxonomist.db.dbmanager`
-----------------------------------------------------

.. automodule:: ncbi_taxonomist.db.dbmanager
  :members:
  :undoc-members:

Database importer: :mod:`ncbi_taxonomist.db.dbimporter`
-------------------------------------------------------
.. automodule:: ncbi_taxonomist.db.dbimporter
  :members:
  :undoc-members:

Database tables
---------------

Base table:  :mod:`ncbi_taxonomist.db.table.basetable`
++++++++++++++++++++++++++++++++++++++++++++++++++++++
.. automodule:: ncbi_taxonomist.db.table.basetable
  :members:
  :undoc-members:

Taxa table: :mod:`ncbi_taxonomist.db.table.taxa`
++++++++++++++++++++++++++++++++++++++++++++++++
.. automodule:: ncbi_taxonomist.db.table.taxa
  :inherited-members:
  :members:
  :undoc-members:

Names table: :mod:`ncbi_taxonomist.db.table.names`
++++++++++++++++++++++++++++++++++++++++++++++++++
.. automodule:: ncbi_taxonomist.db.table.names
  :inherited-members:
  :members:
  :undoc-members:

Accession table: :mod:`ncbi_taxonomist.db.table.accessions`
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.. automodule:: ncbi_taxonomist.db.table.accessions
  :inherited-members:
  :members:
  :undoc-members:
