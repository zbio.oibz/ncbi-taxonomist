.. include:: ../global.rst
.. _models:

Data models
===========

|basename_tt| data models implement taxonomic and accession data. Models use a
:class:`ncbi_taxonomist.model.datamodel.DataModel` as base class.

Basic data model: :mod:`ncbi_taxonomist.model.datamodel`
--------------------------------------------------------

.. automodule:: ncbi_taxonomist.model.datamodel
    :members:
    :undoc-members:

Taxon model: :mod:`ncbi_taxonomist.model.taxon`
-----------------------------------------------
.. inheritance-diagram:: ncbi_taxonomist.model.taxon
.. automodule:: ncbi_taxonomist.model.taxon
    :members:
    :undoc-members:
    :inherited-members:

Accession Data model: :mod:`ncbi_taxonomist.model.accession`
------------------------------------------------------------
.. code-block::shell

  {
    'uid': '1578893816',
    'taxon_id': 11103,
    'db': 'nucleotide',
    'accessions': {
                    'caption': 'MK473203',
                    'accessionversion': 'MK473203.1',
                    'extra': 'gi|1578893816|gb|MK473203.1|'
                  }
  }


.. inheritance-diagram:: ncbi_taxonomist.model.accession
.. automodule:: ncbi_taxonomist.model.accession
    :members:
    :undoc-members:
    :inherited-members:
