.. include::  ../global.rst
.. _entrypoint:

ncbi-taxonomist
===============

This is the entry script for ncbi-taxonomist. It runs the requested command
and checks its parameters.


.. automodule:: ncbi-taxonomist
  :members:
  :undoc-members:
