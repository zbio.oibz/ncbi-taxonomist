.. include:: global.rst
.. _cookbook:

**********************
Cookbook
**********************


.. _resolvelineage:

Resolving lineages
==================

Accession numbers
-----------------
.. code-block:: shell

  ncbi-taxonomist map -a QZWG01000002.1 MG831203.1 LC491622.1 MN069609.1 MH510643.1 | \
  ncbi-taxonomist resolve -m | \
  jq -cr  '[.accs, .data.lin[].name]|@tsv'

For more ``jq`` help, please refer to:

  - `jq manual <https://stedolan.github.io/jq/manual/>`_
  - `Reshaping JSON with jq <https://programminghistorian.org/en/lessons/json-and-jq>`_


Reformat resolve result
-----------------------
.. code-block:: shell

  ncbi-taxonomist resolve -db test.db -t 9606 | \
  jq -r  '.lin[]|"\(.taxon_id) \(.name) \(.rank) \(.parent_id)"'
