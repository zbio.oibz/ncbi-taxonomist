.. include:: global.rst
.. _commands:

********
Commands
********

General notes
=============

All |basename_tt| commands have the following underlying structure:


``ncbi-taxonomist <command> <options>``

Reading from STDIN
------------------

Accessions, taxids, or names can be passed as argument or from STDIN. However,
some commands need to know what is expected as input when reading from STDIN.

When not reading from STDIN, taxid and name arguments can be used together, i.e.

``ncbi-taxonomist collect -t 2 -n human``

Importing into local database
-----------------------------

``ncbi-taxonomist`` imports data into a local database only via STDIN. The
``import`` command imports data while printing its input to STDOUT. This can be
alterd by passing a filter argument which will print only the requested
attribute.

.. include:: commands/map.inc
.. include:: commands/collect.inc
.. include:: commands/import.inc
.. include:: commands/resolve.inc
.. include:: commands/subtree.inc
