.. include:: global.rst

###############
ncbi-taxonomist
###############

Version: |release|

PyPi: |pypiver| https://pypi.org/project/ncbi-taxonomist

Docs updated: |today|

Licence: |basename_tt| is licensed under the `GNU General Public License v3
(GPLv3)`_ or later.

********
Synopsis
********

.. code::

  $ pip install ncbi-taxonomist --user
  $ ncbi-taxonomist collect -n human

|basename_tt| handles and manages phylogenetic data from NCBI Entrez
[Entrez2016]_. It can:

  - **map**: map between taxids, names, and accessions to related taxonomic information

  - **resolve**: resolve lineages for taxa (taxid and names) and accessions, e.g. sequence or protein

  - **import**: store obtained results locally in a SQLite databases

  - **subtree**: extract a whole lineage, or a specific rank, or a range or ranks,
             from a taxid or name
  - **group**: create user defined groups for taxa, for example

    - create a group for all taxa specific for a project
    - group taxa without a phylogenetic relationship, e.g. group all taxa
      representing trees inot a group "trees"

The |basename_tt| commands, e.g. map or import, can be chained together using
pipes to from more complex tasks. For example, to populate a local database
``collect`` will fetch data remotely from Entrez and print it to STDOUT where
``import`` will read ``STDIN`` and populates the local database (see below).

.. code-block:: shell

  ncbi-taxonomist collect -n human | ncbi-taxonomist import -db taxo.db

*******
Install
*******

.. code-block:: shell

  pip install ncbi-taxonomist --user

For more details, see :ref:`install`.

Source code
===========

 - https://gitlab.com/jpb/ncbi-taxonomist

::

  git clone https://gitlab.com/jpb/ncbi-taxonomist.git

Dependencies
============

 |basename_tt| has two dependencies:

  - ``entrezpy``: to handle remote requests to NCBI's Entrez databases

    - https://gitlab.com/ncbipy/entrezpy.git
    - https://pypi.org/project/entrezpy/
    - https://doi.org/10.1093/bioinformatics/btz385


  - ``taxnompy``: to parse taxonomic XML files from NCBI
    - https://gitlab.com/ncbipy/taxonompy.git
    - https://pypi.org/project/taxonompy/

These are libraries maintained by myself and rely solely on the Python standard
library. Therefore, |basename_tt| is less prone to suffer
`dependency hell <https://en.wikipedia.org/wiki/Dependency_hell>`_.

Local databases
===============

To use local databases, `SQLite <https://sqlite.org/index.html>`_ (>= 3.29.0)
has to be installed. |basename_tt| works without local databases, but needs to
fetch all data remotely for each query.

*******
Contact
*******

To report bugs and/or errors, please open an issue at
https://gitlab.com/ncbi-taxonomist or contact me at: jan.buchmann@sydney.edu.au
Of course, feel free to fork the code, improve it, and/or open a pull request.

**********
References
**********

.. .. target-notes::
.. [Entrez2016] https://doi.org/10.1093/nar/gkw1071
.. .. _NCBI: http://www.ncbi.nlm.nih.gov/
.. _GNU General Public License v3 (GPLv3): https://www.gnu.org/licenses/gpl-3.0.en.html

**********************
Full Table of Contents
**********************

.. toctree::
  :maxdepth: 1
  :titlesonly:

  install
  commands
  basics
  cookbook
  module_references




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
