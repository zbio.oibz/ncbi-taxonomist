# This file does only contain a selection of the most common options. For a
# full list see the documentation:
# http://www.sphinx-doc.org/en/master/config

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
import json

sys.path.insert(0, os.path.abspath('.'))
sys.path.insert(0, os.path.abspath('../../../src'))
sys.path.insert(0, os.path.abspath('../../../libs'))
from distutility import distutility


# -- Project information -----------------------------------------------------

project = 'ncbi-taxonomist'
copyright = '2020, Jan P Buchmann'
author = 'Jan P Buchmann'
today_fmt = '%Y-%m-%d'

version = distutility.pypi_version('../../../VERSION.json')
release = distutility.repo_version('../../../VERSION.json')

# -- General configuration ---------------------------------------------------

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.doctest',
    'sphinx.ext.intersphinx',
    'sphinx.ext.todo',
    'sphinx.ext.coverage',
    'sphinx.ext.viewcode',
    'sphinx.ext.inheritance_diagram',
    'sphinx.ext.extlinks',
    #'sphinx_autodoc_typehints'
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']
source_suffix = '.rst'
master_doc = 'index'
language = None
exclude_patterns = []
pygments_style = 'sphinx'
rst_prolog = """
.. |pypiver| replace:: {0}
""".format(version)

# -- Options for HTML output -------------------------------------------------

html_theme = 'alabaster'
html_static_path = ['_static']
html_sidebars = {
  '**': ['localtoc.html', 'globaltoc.html', 'relations.html'],
}
htmlhelp_basename = 'ncbitaxonomistdoc'
# html_theme_options = {}

# -- Options for LaTeX output ------------------------------------------------

latex_elements = {
    # 'papersize': 'a4paper',
    # 'pointsize': '11pt',
    # 'preamble': '',
    # 'figure_align': 'htbp',
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
    (master_doc, 'ncbi-taxonomist.tex', 'ncbi-taxonomist Documentation',
     'Jan P Buchmann', 'manual'),
]

# -- Options for manual page output ------------------------------------------
# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    (master_doc, 'ncbi-taxonomist', 'ncbi-taxonomist Documentation',
     [author], 1)
]

# -- Options for Texinfo output ----------------------------------------------
# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
    (master_doc, 'ncbi-taxonomist', 'ncbi-taxonomist Documentation',
     author, 'ncbi-taxonomist', 'One line description of project.',
     'Miscellaneous'),
]

# -- Options for intersphinx extension ---------------------------------------
# Example configuration for intersphinx: refer to the Python standard library.
intersphinx_mapping = {'https://docs.python.org/': None}

# -- Options for todo extension ----------------------------------------------
# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = True
