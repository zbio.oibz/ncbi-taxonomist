.. include:: global.rst
.. _modulerefs:

*****************
Module references
*****************

Documentation of the different modules and classes used in |basename_tt|.

.. toctree::
  :maxdepth: 3

  module_references/ncbi-taxonomist
  module_references/utils
  module_references/db
  module_references/mapping
  module_references/datamodels
