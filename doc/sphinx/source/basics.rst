.. include:: global.rst
.. _basics:

***************
Basic functions
***************

This section shows the basic usage of |basename_tt|. More complex examples,
inlcuding data extraction with ``jq`` can be found :ref:`here <cookbook>`.

Map
===

The ``map`` command maps taxonomic information for taxids, names, and
accessions. Without specifing the ``-edb`` argument, ``nucleotide`` Entrez
database is assumed.

Supported access Entrez databases
+++++++++++++++++++++++++++++++++

===============   =======
Entrez database   Example
===============   =======
assembly          ``ncbi-taxonomist map -edb assembly -a ASM1001476v1 ViralProj177933``
bioproject        ``ncbi-taxonomist map -edb bioproject -a PRJNA604394``
nucleotide        ``ncbi-taxonomist map -edb nucleotide -a MH510449.1``

                  ``ncbi-taxonomist map -a MH510449.1``
protein           ``ncbi-taxonomist map -a YP_009345145 -edb protein``
===============   =======

Work in progress
++++++++++++++++

  Quering the following databases does not return the queried accession in the
  results. Therefore, results cannot identify which accession corresponds to
  which rsults if more than one are requested. To solve the one-to-one
  relationship, each of the accessions from these databases needs to be queried
  one-by-one and not as batch query. This problem will be solved after the
  initial first

 - biosample
 - biosystems
 - cdd
 - dbvar
 - gap
 - gapplus
 - gene
 - genome

..  note::

  - geoprofiles: using accessions like ``GDS6063`` should work
  - proteinclusters: ``commontaxonomy`` attribute can be used as name
  - sra: Only XML results. Needs a dedictaed parser

Taxids and names
----------------

Find taxonomic information for two taxids  and two names. Taxids and names can
be separated by commas and/or space. However, names containing space need to be
encapsulated by ``'``

..  code-block:: shell

  $ ncbi-taxonomist map -t 562, 10508 -n man 'Influenza B virus (B/Acre/121609/2012)', chimpanzee

The output is a JSON object for each taxid or name on a new line. Every object
has two values, the requested name or taxid and the correspondong taxonomic data
as ``taxon`` object.

..  code-block:: json

  {"name":"Influenza B virus (B/Acre/121609/2012)","taxon":{"taxon_id":1334390,"rank":null,"names":{"Influenza B virus (B/Acre/121609/2012)":"scientific_name"},"parent_id":11520,"name":"Influenza B virus (B/Acre/121609/2012)"}}
  {"name":"man","taxon":{"taxon_id":9606,"rank":"species","names":{"Homo sapiens":"scientific_name","human":"GenbankCommonName","man":"CommonName"},"parent_id":9605,"name":"Homo sapiens"}}
  {"name":"chimpanzee","taxon":{"taxon_id":9598,"rank":"species","names":{"Pan troglodytes":"scientific_name","chimpanzee":"GenbankCommonName"},"parent_id":9596,"name":"Pan troglodytes"}}
  {"taxid":562,"taxon":{"taxon_id":562,"rank":"species","names":{"Escherichia coli":"scientific_name","Bacillus coli":"Synonym","Bacterium coli":"Synonym","Bacterium coli commune":"Synonym","Enterococcus coli":"Synonym","E. coli":"CommonName","Escherichia sp. 3_2_53FAA":"Includes","Escherichia sp. MAR":"Includes","bacterium 10a":"Includes","bacterium E3":"Includes","Escherichia/Shigella coli":"EquivalentName","ATCC 11775":"type material","ATCC:11775":"type material","BCCM/LMG:2092":"type material","CCUG 24":"type material","CCUG 29300":"type material","CCUG:24":"type material","CCUG:29300":"type material","CIP 54.8":"type material","CIP:54.8":"type material","DSM 30083":"type material","DSM:30083":"type material","IAM 12119":"type material","IAM:12119":"type material","JCM 1649":"type material","JCM:1649":"type material","LMG 2092":"type material","LMG:2092":"type material","NBRC 102203":"type material","NBRC:102203":"type material","NCCB 54008":"type material","NCCB:54008":"type material","NCTC 9001":"type material","NCTC:9001":"type material"},"parent_id":561,"name":"Escherichia coli"}}
  {"taxid":10508,"taxon":{"taxon_id":10508,"rank":"family","names":{"Adenoviridae":"scientific_name"},"parent_id":10239,"name":"Adenoviridae"}}

Map accession
=============
